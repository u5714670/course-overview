#!/usr/bin/env python3
# COMP1040 The Craft of Computing
# Code from demonstration lecture.

__author__    = "Stephen Gould and Mark Reid"
__email__     = "comp1040@cs.anu.edu.au"
__copyright__ = "Copyright 2015, The Australian National University" 

# --- Reading the Data ---------------------------------------------------

import json

# Read the database of actors and the movies that they appear in,
# and store as a map of movie titles to actors.

movie_cast = dict()

with open('acting.json') as file:
    data = json.load(file)
    for actor, movies in data.items():
        for title in movies:
            if title not in movie_cast:
                movie_cast[title] = list()
            movie_cast[title].append(actor)

# --- Find Most Common Co-Star for a Given Actor -------------------------

# write a function for finding the costars of a given actor.
def find_costars(star):
    costars = []
    for title, actors in movie_cast.items():
        if star in actors:
            print("{} stars in {}".format(star, title))
            for actor in actors:
                if actor != star and actor not in costars:
                    costars.append(actor)
    return costars

# test the function with "Tom Cruise"
star = "Cruise, Tom"
costars = sorted(find_costars(star))
print("Co-stars of {}: {}".format(star, '; '.join(costars)))

# write a function that finds movies in common between two actors
def movies_in_common(star, costar):
    movies = []
    for title, actors in movie_cast.items():
        if star in actors and costar in actors:
            movies.append(title)
    return movies

# for each costar of the star, find out how many movies they have in common
shared_movie_count = {}
for costar in costars:
    shared_movie_count[costar] = len(movies_in_common(star, costar))

# find the costar with most movies in common and show those movies
max_movies = max(shared_movie_count.values())
for costar, count in shared_movie_count.items():
    if count == max_movies:
        print("{} stars with {} {} times".format(costar, star, count))
        print("   in {}".format(movies_in_common(star, costar)))

# --- Visualization ------------------------------------------------------

import networkx as nx
import matplotlib.pyplot as plt

# extract the set of actors from movie cast
all_actors = set([actor for cast in movie_cast.values() for actor in cast])

# create a graph and add the actors as nodes
g = nx.Graph()
for a in all_actors:
    g.add_node(a)

# add edges between actors appearing together in the same movie
for title, actors in movie_cast.items():
    for i in range(len(actors)):
        for j in range(i + 1, len(actors)):
            g.add_edge(actors[i], actors[j])

print("Actor graph has {} nodes and {} edges.".format(len(g.nodes()), len(g.edges())))

# draw and show the graph
nx.draw(g, node_color="#ff7f7f", node_size=50, edge_color="#000000", width=1, with_labels=False)
plt.show()

# --- Visualization II ---------------------------------------------------

# find the shortest path between two actors
first_actor = "Cruise, Tom"
second_actor = "Hackman, Gene"

path = nx.shortest_path(g, first_actor, second_actor)

# draw and show path on the graph
path_edges = list(zip(path, path[1:]))
node_labels = dict([(actor, actor) for actor in path])

positions = nx.spring_layout(g)
nx.draw(g, positions, edge_color="#7f7f7f", node_size=50, width=1, alpha=0.25)
nx.draw_networkx_edges(g, positions, edgelist=path_edges, edge_color="#0000ff", width=4)
nx.draw_networkx_nodes(g, positions, nodelist=path, node_color="#7f7fff", node_size=50)
nx.draw_networkx_labels(g, positions, labels=node_labels)
plt.show()
