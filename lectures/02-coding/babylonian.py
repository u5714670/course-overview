#!/usr/bin/env python3
# COMP1040 The Craft of Computing
#
# Babylonian algorithm for calculating square roots

def square_root(x):
    y = 0.5 * x
    while abs(y * y - x) > 1.0e-6:
        y = 0.5 * (y + x / y)
    return y

# test cases
print("The square root of 4 is {}".format(square_root(4)))
print("The square root of 2 is {}".format(square_root(2.0)))
