# COMP1040 The Craft of Computing
Semester 2, 2015

![](craft.png)

## Overview

Knowing how to effectively use computational tools to perform data
analysis, simulation, and visualisation is rapidly becoming a crucial
skill in an increasing number of industries and academic
disciplines. The aim of this course is to provide skills for tackling
the "messiness" of real-world computer systems, programming languages,
and data. Unlike many other computer science courses which explain a
single area in depth, the focus of The Craft of Computing
<sup>\[[1](#art)\]</sup> will be on understanding core principles that
allow students to quickly and confidently learn and apply a variety of
computational tools to several different types of problem. The skills
developed in this course will be a great asset to students in their
undergraduate life and future careers.

<a name="art">\[1\]</a>: A companion course, The Art of Computing, teaches
computational *thinking* in contrast to computational *doing* taught
in this course.

## Learning Outcomes

Students completing this class will:

* Appreciate the creative possibilities computation brings to multiple disciplines.
* Be able to solve practical problems in various domains using appropriate software tools.
* Be able to understand, modify, debug, and write small programs in a high-level programming language.
* Be able to translate learned programming skills to new programming languages, tools, and contexts.
* Understand the culture, conventions and resources to do with software development, deployment, and use.

## What this Course is Not

This course is about craft, not science---it is about using tools, not
building them. It is not intended as a substitute for introductory
computer science courses that systematically introduce students to a
language so they will be able to write robust, performant code from
scratch. Instead, we want to show students how to adapt existing code
and/or write small amounts of simple data processing code that ``glues''
together existing libraries to solve a problem. Some of this
philosophy is captured by the principles of
[post-modern programming](\href{http://c2.com/cgi/wiki?PostModernProgramming).

This course is also not about programming, although programming is an
important skill, it is only part of the picture. The environments,
tools, existing libraries, and cultures around solving computational
problems are of equal or greater importance. It is not necessary to
understand ever aspect of a programming langauge to be able to use it
to solve a problem at hand.

## Topics

The emphasis of this course of **getting things done with computers**
is reflected in the following choice of topics:

* Introduction to computational environments and tools.
* Computer fundamentals and computational thinking.
* Learning programming languages and libraries.
* Data flow and visualisation.
* Collaborating and distributing code.

## Course Snapshot

The workload model for the course assumes that an average student
spending 10 hours per week on the course should be capable of
achieveing a Credit level grade. Contact hours include three 50 minute
lectures and one laboratory session per week. In addition lectures and
tutors will hold office hours and be available online for help with
weekly exercises and assignments.

![](course_snapshot.png)
